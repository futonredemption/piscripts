import json
import os

def LoadConfig(config_path):
  default_conf_data = {
    'width': 1920,
    'height': 1080,
    'fps': 30,
    'output': 'video-out' 
  }
  
  conf_data = default_conf_data.copy()

  if os.path.exists(config_path):
    with open(config_path) as conf:
      file_data = json.loads(conf.read())
    with open(config_path, 'w') as conf:
      conf.write(json.dumps(file_data, sort_keys=True, indent=2, separators=(',', ': ')))
      conf_data.update(file_data)
  return conf_data
