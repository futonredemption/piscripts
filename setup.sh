#!/bin/bash

ROOT_DIR=${HOME}/opencv2-install
CV_VER=2.4.11
sudo apt-get update
sudo apt-get -q -y upgrade
sudo apt-get -q -y install \
  build-essential cmake pkg-config \
  libjpeg8-dev libtiff5-dev libjasper-dev libpng12-dev \
  libgtk2.0-dev \
  libavcodec-dev libavformat-dev libswscale-dev libv4l-dev \
  libatlas-base-dev gfortran \
  python2.7-dev \
  python-pip \
  python-picamera

sudo pip install virtualenv virtualenvwrapper
sudo rm -rf ~/.cache/pip

cat <<EOT >> ${HOME}/.profile
export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh
EOT

source ${HOME}/.profile

mkdir -p ${ROOT_DIR}
cd ${ROOT_DIR}
wget -O opencv-${CV_VER}.zip http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/${CV_VER}/opencv-${CV_VER}.zip/download
unzip opencv-${CV_VER}.zip
mkdir -p ${ROOT_DIR}/opencv-${CV_VER}/build
cd ${ROOT_DIR}/opencv-${CV_VER}/build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local \
  -D BUILD_NEW_PYTHON_SUPPORT=ON -D INSTALL_C_EXAMPLES=ON \
  -D INSTALL_PYTHON_EXAMPLES=ON  -D BUILD_EXAMPLES=ON ..

make
sudo make install
sudo ldconfig
cd ~/.virtualenvs/cv/lib/python2.7/site-packages/
ln -s /usr/local/lib/python2.7/site-packages/cv2.so cv2.so
ln -s /usr/local/lib/python2.7/site-packages/cv.py cv.py


mkvirtualenv cv
workon cv
pip install imutils
pip install picamera numpy
