import cv2

def main():
  image = cv2.imread("jurassic-park-tour-jeep.jpg")
  print image.shape
  r = 100.0 / image.shape[1]
  dim = (100, int(image.shape[0] * r))
   
  # perform the actual resizing of the image and show it
  resized = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
  cv2.imwrite("resized.jpg", resized)

if __name__ == "__main__":
  main()
