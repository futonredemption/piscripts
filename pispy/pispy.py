import argparse
import config
import camera

def main():
  ap = argparse.ArgumentParser()
  ap.add_argument('-c', '--config', default='conf.json', help='Configuration File')
  args = ap.parse_args()
  conf = config.LoadConfig(args.config)
  camera.CaptureVideo(args, conf)



if __name__ == "__main__":
  main()
