#!/bin/bash

#http://stuffbabiesneed.com/blog/building-raspberry-pi-baby-monitor-part-one/
echo "Baby Monitor"

sudo apt-get update
sudo apt-get -qq -y upgrade
sudo apt-get -qq -y install motion

sudo cat <<EOL > /etc/default/motion
start_motion_daemon=yes
EOL

sudo cat <<EOL >> /etc/motion/motion.conf
daemon on
framerate 2 
width 640
height 480
ffmpeg_video_codec mpeg4 

stream_localhost off
webcam_localhost off
control_localhost off
webcontrol_localhost off
webcam_port 8081
EOL


sudo service motion start

/sbin/ifconfig | grep inet

# sudo modprobe bcm2835-v4l2
# cvlc v4l2:///dev/video0 --v4l2-width 1920 --v4l2-height 1080 --v4l2-chroma h264 --sout '#standard{access=http,mux=ts,dst=0.0.0.0:12345}' :demux=264