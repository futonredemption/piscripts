from picamera import PiCamera
import os

def CaptureVideo(args, conf):
  print args
  print conf
  camera = PiCamera()
  camera.resolution = (conf['width'], conf['height'])
  camera.framerate = conf['fps']
  camera.led = False
  camera.exposure_mode = 'night'
  camera.annotate_frame_num = True
  camera.annotate_text ='cam'
  
  capture_dir = os.path.join(conf['output'], 'captures')
  os.makedirs(capture_dir)
  capture_path = os.path.join(capture_dir, 'test.jpg')
  camera.capture(capture_path)
  camera.close()
