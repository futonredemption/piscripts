#!/bin/bash
# http://www.pyimagesearch.com/2015/10/26/how-to-install-opencv-3-on-raspbian-jessie/
CV_VER=3.1.0
ROOT_DIR=${HOME}/opencv3-install
sudo apt-get update
sudo apt-get -q -y upgrade
sudo apt-get -q -y install build-essential git cmake pkg-config \
  libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev \
  libavcodec-dev libavformat-dev libswscale-dev libv4l-dev \
  libxvidcore-dev libx264-dev \
  libgtk2.0-dev \
  libatlas-base-dev gfortran \
  python2.7-dev python3-dev \
  python-pip python3-pip

mkdir -p ${ROOT_DIR}
cd ${ROOT_DIR}
wget -O opencv.zip https://github.com/Itseez/opencv/archive/${CV_VER}.zip
unzip opencv.zip
wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/${CV_VER}.zip
unzip opencv_contrib.zip

sudo pip install virtualenv virtualenvwrapper
sudo rm -rf ~/.cache/pip

mkdir -p ${ROOT_DIR}/opencv-${CV_VER}/build

cd ${ROOT_DIR}/opencv-${CV_VER}/build
cmake -D CMAKE_BUILD_TYPE=RELEASE \
  -D CMAKE_INSTALL_PREFIX=/usr/local \
  -D INSTALL_C_EXAMPLES=OFF \
  -D INSTALL_PYTHON_EXAMPLES=ON \
  -D OPENCV_EXTRA_MODULES_PATH=${ROOT_DIR}/opencv_contrib-${CV_VER}/modules \
  -D BUILD_EXAMPLES=ON ..

	
make -j4
sudo make install
sudo ldconfig

ls -l /usr/local/lib/python2.7/site-packages/

cd ~/.virtualenvs/cv/lib/python2.7/site-packages/
ln -s /usr/local/lib/python2.7/site-packages/cv2.so cv2.so